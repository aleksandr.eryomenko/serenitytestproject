# Waarkoop API Test Project

This is a test automation project

## Common tech stack to be installed

For building this project it is necessary to install the following:

- Installed Java 8 ([Java Downloads](http://jdk.java.net/java-se-ri/8-MR3))
- Installed Git ([Git Downloads](https://git-scm.com/downloads))
- Installed Maven 3+ ([Maven Downloads](https://maven.apache.org/download.cgi))

## Set up and running tests

```
   $ https://gitlab.com/aleksandr.eryomenko/serenitytestproject.git
   $ mvn clean verify
```
Also test run can be customized to use specific test or feature, for example

```
   $ mvn clean verify "-Dcucumber.options= --tags @test"
   $ mvn clean verify "-Dcucumber.options= --tags @searchProduct"
```

In both cases we have automatically generated Allure report, for more details use this link
([Allure Report](https://docs.qameta.io/allure/))

```
   $ mvn allure:report
```
After executing report will be available in build directory and can be opened through index.html file in any of browsers:
```
   $ /target/site/allure-maven-plugin
```

## That is what was refactored in the project
- Refactored Maven pom.xml
- Restructured base test framework components (API Client/Service layer, DTOs layer)
- Removed unnecessary Gradle files
- Removed unused config files
- Removed .idea folder from repository
- Removed target folder from repository
- Added default serenity config file
- Extended step definitions clas with additional steps and assertions
- Added reporting tool - Allure
- Added .gitignore file
- Implemented CI/CD process in .gitlab-ci.yml with Allure report

## Allure Report
Allure report generated - https://aleksandr.eryomenko.gitlab.io/-/serenitytestproject/-/jobs/5045073948/artifacts/public/index.html