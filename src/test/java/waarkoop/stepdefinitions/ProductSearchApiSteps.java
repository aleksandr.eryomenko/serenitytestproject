package waarkoop.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import net.thucydides.core.annotations.Steps;
import org.apache.http.HttpStatus;
import waarkoop.api.services.ProductsAPIService;
import waarkoop.models.responses.ProductSearchErrorDetailsResponse;
import waarkoop.models.responses.ProductSearchErrorEmptyResponse;
import waarkoop.models.responses.ProductSearchErrorResponse;
import waarkoop.utils.URLMatcher;
import waarkoop.models.responses.ProductSearchResponse;

import java.util.Arrays;

import static javax.servlet.http.HttpServletResponse.SC_OK;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class ProductSearchApiSteps {

    private Response apiProductSearchResponse;
    private final String scriptText = "<script>\"sdfsd\"</script>";

    @Steps
    public ProductsAPIService productsAPIService;

    @When("user executes GET request to retrieve product list items by name {string}")
    public void the_user_executes_get_request_to_retrieve_product_list_items_by_name(String validProductName) {
        apiProductSearchResponse = productsAPIService.searchProductByName(validProductName);
    }

    @Then("user sees available product list items for product with 200 status code")
    public void the_user_sees_available_product_list_items_for_product_with_200() {
        restAssuredThat(validatableResponse -> apiProductSearchResponse.then()
                                                            .statusCode(SC_OK));

        ProductSearchResponse[] successProductSearchResponses = apiProductSearchResponse.as(ProductSearchResponse[].class);

        Arrays.stream(successProductSearchResponses).forEach(product -> {
            assertThat(reason("provider"), product.getProvider(), notNullValue());
            assertThat(reason("title"), product.getTitle(), notNullValue());
            assertThat(reason("url"), product.getUrl(), anyOf(notNullValue(), URLMatcher.isValidURL()));
            assertThat(reason("brand"), product.getBrand(), anyOf(nullValue(), notNullValue()));
            assertThat(reason("price"), product.getPrice(), notNullValue());
            assertThat(reason("unit"), product.getUnit(), notNullValue());
            assertThat(reason("isPromo"), product.getIsPromo(), notNullValue());
            assertThat(reason("promoDetails"), product.getPromoDetails(), anyOf(emptyString(), notNullValue()));
            assertThat(reason("image"), product.getImage(), allOf(notNullValue(), URLMatcher.isValidURL()));
        });
    }

    @Then("user does not see product list items by name {string} with 404 status code")
    public void the_user_does_not_see_product_list_items_by_name(String productName) {
        restAssuredThat(validatableResponse -> apiProductSearchResponse.then()
                                                            .statusCode(HttpStatus.SC_NOT_FOUND));

        ProductSearchErrorResponse errorProductSearchResponse = apiProductSearchResponse.as(ProductSearchErrorResponse.class);
        ProductSearchErrorDetailsResponse errorDetails = errorProductSearchResponse.getDetail();

        assertThat("Invalid error value!", errorDetails.getError(), is(true));
        assertThat("Invalid message value!", errorDetails.getMessage(), is("Not found"));
        assertThat("Invalid requested item value!", errorDetails.getRequested_item(), is(productName));
        assertThat("Invalid served by value!", errorDetails.getServed_by(), is("https://waarkoop.com"));
    }

    @Then("user sees empty product details response body with 200 status code")
    public void the_user_sees_empty_product_details_response_body_with_200() {
        restAssuredThat(validatableResponse -> apiProductSearchResponse.then()
                                                            .statusCode(HttpStatus.SC_OK));

        assertThat("Response body is not empty!", apiProductSearchResponse.asString(), is("[]"));
    }

    @Then("user is getting not authenticated error in response body with 401 status code")
    public void user_is_getting_not_authenticated_error_in_response_body_with_401() {
        restAssuredThat(response -> apiProductSearchResponse.then()
                .statusCode(HttpStatus.SC_UNAUTHORIZED));

        ProductSearchErrorEmptyResponse productSearchErrorEmptyResponse = apiProductSearchResponse.as(ProductSearchErrorEmptyResponse.class);

        assertThat("Invalid detail message!", productSearchErrorEmptyResponse.getDetail(), is("Not authenticated"));
    }

    @When("user executes GET product request with script text")
    public void the_user_executes_get_request_with_script() {
        apiProductSearchResponse = productsAPIService.searchProductByName(scriptText);
    }

    @Then("user does not see product list items by typing script with 404 status code")
    public void the_user_does_not_see_product_list_items_by_typing_script() {
        restAssuredThat(validatableResponse -> apiProductSearchResponse.then()
                .statusCode(HttpStatus.SC_NOT_FOUND));

        ProductSearchErrorEmptyResponse productSearchErrorEmptyResponse = apiProductSearchResponse.as(ProductSearchErrorEmptyResponse.class);

        assertThat("Invalid detail message!", productSearchErrorEmptyResponse.getDetail(), is("Not Found"));
    }
    private String reason(String parameter) {
        return String.format("Parameter - '%s' was not found!", parameter);
    }
}