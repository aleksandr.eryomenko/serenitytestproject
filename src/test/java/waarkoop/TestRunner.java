package waarkoop;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(monochrome = true,
        plugin = {"html:target/cucumber-reports", "json:target/cucumber.json", "pretty"},
        features = "src/test/resources/features",
        tags = "@productSearch"
//        tags = "@test"
)
public class TestRunner {
}

