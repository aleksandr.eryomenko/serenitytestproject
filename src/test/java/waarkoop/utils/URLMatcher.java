package waarkoop.utils;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.net.URI;
import java.net.URISyntaxException;

public class URLMatcher extends TypeSafeMatcher<String> {

    @Override
    protected boolean matchesSafely(String url) {
        try {
            new URI(url);
            return true;
        } catch (URISyntaxException e) {
            return false;
        }
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("URL is not valid!");
    }

    public static Matcher<String> isValidURL() {
        return new URLMatcher();
    }
}