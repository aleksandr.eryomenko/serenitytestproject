package waarkoop.models.responses;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public class ProductSearchErrorResponse {
    private ProductSearchErrorDetailsResponse detail;
}
