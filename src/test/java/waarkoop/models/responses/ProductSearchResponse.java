package waarkoop.models.responses;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public class ProductSearchResponse {
    private String provider;
    private String title;
    private String url;
    private String brand;
    private Double price;
    private String unit;
    private Boolean isPromo;
    private String promoDetails;
    private String image;
}
