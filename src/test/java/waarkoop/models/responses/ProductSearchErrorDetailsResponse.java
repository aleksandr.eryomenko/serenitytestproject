package waarkoop.models.responses;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Accessors(chain = true)
public class ProductSearchErrorDetailsResponse {
    private Boolean error;
    private String message;
    private String requested_item;
    private String served_by;
}
