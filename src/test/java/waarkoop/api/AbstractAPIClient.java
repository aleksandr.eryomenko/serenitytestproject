package waarkoop.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.config.ObjectMapperConfig;
import io.restassured.config.RestAssuredConfig;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.specification.RequestSpecification;
import net.serenitybdd.rest.SerenityRest;
import waarkoop.utils.ConfigParser;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;
import static io.restassured.http.ContentType.JSON;

public abstract class AbstractAPIClient {

    abstract public String getApiPath();

    public RequestSpecification setUp() {
        SerenityRest.filters(new AllureRestAssured());
        return SerenityRest.given()
                           .baseUri(ConfigParser.getPropertyByName("api-config.baseUrl"))
                           .contentType(JSON)
                           .accept(JSON);
    }
}