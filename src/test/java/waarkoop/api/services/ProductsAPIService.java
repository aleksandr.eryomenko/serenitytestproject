package waarkoop.api.services;

import io.restassured.response.Response;
import net.thucydides.core.annotations.Step;
import waarkoop.api.AbstractAPIClient;

import static java.lang.String.format;

public class ProductsAPIService extends AbstractAPIClient {
    private static String apiPath = "/search/demo/%s";

    @Step
    public Response searchProductByName(String productName) {
        return setUp()
            .basePath(format(apiPath, productName))
            .get();
    }

    @Override
    public String getApiPath() {
        return apiPath;
    }
}