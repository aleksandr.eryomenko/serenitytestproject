@productSearch
Feature: Search product

  Scenario Outline: Check successful searching existing products by valid name
    When user executes GET request to retrieve product list items by name "<validProductName>"
    Then user sees available product list items for product with 200 status code
    Examples:
      | validProductName |
      | pasta            |
      | cola             |
      | apple            |

  Scenario Outline: Check searching existing products by valid name with empty response body
    When user executes GET request to retrieve product list items by name "<productWithEmptyDetails>"
    Then user sees empty product details response body with 200 status code
    Examples:
      | productWithEmptyDetails |
      | orange                  |

  Scenario Outline: Check error getting when user searches products by invalid name
    When user executes GET request to retrieve product list items by name "<invalidProductName>"
    Then user does not see product list items by name "<invalidProductName>" with 404 status code
    Examples:
      | invalidProductName |
      | Pasta              |
      | Cola               |
      | Apple              |
      | orangee            |
      | NULL               |
      | null               |
      | 0                  |
      | 1                  |
      | %$%^&*             |

  Scenario Outline: Check error getting when user searches product with empty or slash name
    When user executes GET request to retrieve product list items by name "<productName>"
    Then user is getting not authenticated error in response body with 401 status code
    Examples:
      | productName |
      |             |
      | ////        |

  Scenario: Check error getting when user search some script
    When user executes GET product request with script text
    Then user does not see product list items by typing script with 404 status code